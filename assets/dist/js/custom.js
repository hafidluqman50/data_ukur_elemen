// $(function(){
	function rupiah_format(string){
		if (Math.sign(string) !== -1) {
			reverse = string.toString().split('').reverse().join(''),
			ribuan 	= reverse.match(/\d{1,3}/g);
			ribuan	= 'Rp. '+ribuan.join('.').split('').reverse().join('')+',00';
			return ribuan;
		}
		return 'Rp. 0';
	}

	$("#image").change(function(){
		var file = document.getElementById("image").files[0];
		var readImg = new FileReader();
		readImg.readAsDataURL(file);
		readImg.onload = function(e) {
			$('#uploadPreview').attr('src',e.target.result).fadeIn();
		}
	});
// });
$(function(){
	// $('body').on('keydown','input,select,textarea',function(e){
	// 	var self = $(this),
	// 		form = self.parents('form:eq(0)'),
	// 		focusable,
	// 		next
	// 		;
	// 	if (e.keyCode == 13) {
	// 		focusable = form.find('input,a,select,button,textarea').filter(':visible');
	// 		console.log(focusable);
	// 		next = focusable.eq(focusable.index(this)+1);
	// 		if (next.length) {
	// 			next.focus();
	// 		}
	// 		else {
	// 			next.submit();
	// 		}
	// 		return false;
	// 	}
	// });

    if ($('.hasil-uji-input').length > 1) {
        $('.hapus-input').show();
    }

    $('.tambah-input').click(function(){
        if ($('#hasil-uji-input').length != 0) {
    		$('.hasil-uji-input:last').find('.parameter_ukur').select2('destroy');
            $('#hasil-uji-input').clone().appendTo('#mantap-gan').find('input').val('');
            $('.hasil-uji-input:last').find('.parameter_ukur').select2();
        }
        $('.hapus-input').show();
    });

    $('.hapus-input').click(function(){
        if ($('.hasil-uji-input').length != 0) {
             $('.hasil-uji-input:last').remove().eq(0);
        }
        if ($('.hasil-uji-input').length == 1) {
            $(this).hide();
        }
    });
	
	$('#sip').click(function(){
		if ($(this).is(':checked')) {
			$('input[name="username"]').removeAttr('disabled');
		}
		else {
			$('input[name="username"]').attr('disabled','disabled');
		}
	});

	$('#check').click(function(){
		if ($(this).is(':checked')) {
			$('input[name="nomor_batch"]').removeAttr('disabled');
		}
		else {
			$('input[name="nomor_batch"]').attr('disabled','disabled');
		}
	});
});