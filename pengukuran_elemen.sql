-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Nov 27, 2019 at 04:19 PM
-- Server version: 8.0.13
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pengukuran_elemen`
--

-- --------------------------------------------------------

--
-- Table structure for table `jenis_air`
--

CREATE TABLE `jenis_air` (
  `id_jenis_air` int(11) NOT NULL,
  `nama_jenis_air` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jenis_air`
--

INSERT INTO `jenis_air` (`id_jenis_air`, `nama_jenis_air`) VALUES
(1, 'Sungai'),
(2, 'Danau');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi_air`
--

CREATE TABLE `lokasi_air` (
  `id_lokasi_air` int(11) NOT NULL,
  `id_jenis_air` int(11) NOT NULL,
  `nama_lokasi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lokasi_air`
--

INSERT INTO `lokasi_air` (`id_lokasi_air`, `id_jenis_air`, `nama_lokasi`) VALUES
(2, 1, 'Sungai Mahakam');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi_udara`
--

CREATE TABLE `lokasi_udara` (
  `id_lokasi_udara` int(11) NOT NULL,
  `nama_lokasi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lokasi_udara`
--

INSERT INTO `lokasi_udara` (`id_lokasi_udara`, `nama_lokasi`) VALUES
(1, 'Lokasi A');

-- --------------------------------------------------------

--
-- Table structure for table `parameter_pengukuran`
--

CREATE TABLE `parameter_pengukuran` (
  `id_parameter_pengukuran` int(11) NOT NULL,
  `parameter_pengukuran` varchar(70) NOT NULL,
  `satuan_ukur` varchar(10) NOT NULL,
  `baku_mutu` double NOT NULL,
  `ket_ukur` varchar(10) NOT NULL,
  `status_data` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parameter_pengukuran`
--

INSERT INTO `parameter_pengukuran` (`id_parameter_pengukuran`, `parameter_pengukuran`, `satuan_ukur`, `baku_mutu`, `ket_ukur`, `status_data`) VALUES
(2, 'pH', '-', 0.03, 'air', 1),
(3, 'CO', 'mg/l', 10, 'udara', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pengukuran_air`
--

CREATE TABLE `pengukuran_air` (
  `id_pengukuran_air` int(11) NOT NULL,
  `id_titik_air` int(11) NOT NULL,
  `nomor_uji` varchar(100) NOT NULL,
  `tahun` year(4) NOT NULL,
  `bulan` varchar(20) NOT NULL,
  `dokumentasi` text CHARACTER SET utf8 COLLATE utf8_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengukuran_air`
--

INSERT INTO `pengukuran_air` (`id_pengukuran_air`, `id_titik_air`, `nomor_uji`, `tahun`, `bulan`, `dokumentasi`) VALUES
(15, 2, '008.035', 2019, 'Januari', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pengukuran_air_detail`
--

CREATE TABLE `pengukuran_air_detail` (
  `id_pengukuran_air_detail` int(11) NOT NULL,
  `id_pengukuran_air` int(11) NOT NULL,
  `id_parameter_pengukuran` int(11) NOT NULL,
  `hasil_uji` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengukuran_air_detail`
--

INSERT INTO `pengukuran_air_detail` (`id_pengukuran_air_detail`, `id_pengukuran_air`, `id_parameter_pengukuran`, `hasil_uji`) VALUES
(4, 15, 2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `pengukuran_udara`
--

CREATE TABLE `pengukuran_udara` (
  `id_pengukuran_udara` int(11) NOT NULL,
  `id_titik_udara` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `bulan` varchar(20) NOT NULL,
  `nomor_uji` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengukuran_udara`
--

INSERT INTO `pengukuran_udara` (`id_pengukuran_udara`, `id_titik_udara`, `tahun`, `bulan`, `nomor_uji`) VALUES
(1, 1, 2019, 'Januari', '009.041');

-- --------------------------------------------------------

--
-- Table structure for table `pengukuran_udara_detail`
--

CREATE TABLE `pengukuran_udara_detail` (
  `id_pengukuran_udara_detail` int(11) NOT NULL,
  `id_pengukuran_udara` int(11) NOT NULL,
  `id_parameter_pengukuran` int(11) NOT NULL,
  `hasil_uji` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengukuran_udara_detail`
--

INSERT INTO `pengukuran_udara_detail` (`id_pengukuran_udara_detail`, `id_pengukuran_udara`, `id_parameter_pengukuran`, `hasil_uji`) VALUES
(1, 1, 3, 20);

-- --------------------------------------------------------

--
-- Table structure for table `titik_air`
--

CREATE TABLE `titik_air` (
  `id_titik_air` int(11) NOT NULL,
  `id_lokasi_air` int(11) NOT NULL,
  `nama_titik` varchar(100) NOT NULL,
  `letak_titik` text NOT NULL,
  `dokumentasi` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `titik_air`
--

INSERT INTO `titik_air` (`id_titik_air`, `id_lokasi_air`, `nama_titik`, `letak_titik`, `dokumentasi`) VALUES
(2, 2, '', 'A - B', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `titik_udara`
--

CREATE TABLE `titik_udara` (
  `id_titik_udara` int(11) NOT NULL,
  `id_lokasi_udara` int(11) NOT NULL,
  `nama_titik` varchar(100) NOT NULL,
  `letak_titik` varchar(100) NOT NULL,
  `dokumentasi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `titik_udara`
--

INSERT INTO `titik_udara` (`id_titik_udara`, `id_lokasi_udara`, `nama_titik`, `letak_titik`, `dokumentasi`) VALUES
(1, 1, '', 'A - B', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jenis_air`
--
ALTER TABLE `jenis_air`
  ADD PRIMARY KEY (`id_jenis_air`);

--
-- Indexes for table `lokasi_air`
--
ALTER TABLE `lokasi_air`
  ADD PRIMARY KEY (`id_lokasi_air`),
  ADD KEY `id_jenis_air` (`id_jenis_air`);

--
-- Indexes for table `lokasi_udara`
--
ALTER TABLE `lokasi_udara`
  ADD PRIMARY KEY (`id_lokasi_udara`);

--
-- Indexes for table `parameter_pengukuran`
--
ALTER TABLE `parameter_pengukuran`
  ADD PRIMARY KEY (`id_parameter_pengukuran`);

--
-- Indexes for table `pengukuran_air`
--
ALTER TABLE `pengukuran_air`
  ADD PRIMARY KEY (`id_pengukuran_air`),
  ADD KEY `id_titik_air` (`id_titik_air`);

--
-- Indexes for table `pengukuran_air_detail`
--
ALTER TABLE `pengukuran_air_detail`
  ADD PRIMARY KEY (`id_pengukuran_air_detail`),
  ADD KEY `id_pengukuran_air` (`id_pengukuran_air`),
  ADD KEY `id_parameter` (`id_parameter_pengukuran`);

--
-- Indexes for table `pengukuran_udara`
--
ALTER TABLE `pengukuran_udara`
  ADD PRIMARY KEY (`id_pengukuran_udara`);

--
-- Indexes for table `pengukuran_udara_detail`
--
ALTER TABLE `pengukuran_udara_detail`
  ADD PRIMARY KEY (`id_pengukuran_udara_detail`),
  ADD KEY `id_pengukuran_udara` (`id_pengukuran_udara`),
  ADD KEY `id_parameter_pengukuran` (`id_parameter_pengukuran`);

--
-- Indexes for table `titik_air`
--
ALTER TABLE `titik_air`
  ADD PRIMARY KEY (`id_titik_air`),
  ADD KEY `id_lokasi_air` (`id_lokasi_air`);

--
-- Indexes for table `titik_udara`
--
ALTER TABLE `titik_udara`
  ADD PRIMARY KEY (`id_titik_udara`),
  ADD KEY `id_lokasi_udara` (`id_lokasi_udara`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jenis_air`
--
ALTER TABLE `jenis_air`
  MODIFY `id_jenis_air` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lokasi_air`
--
ALTER TABLE `lokasi_air`
  MODIFY `id_lokasi_air` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lokasi_udara`
--
ALTER TABLE `lokasi_udara`
  MODIFY `id_lokasi_udara` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `parameter_pengukuran`
--
ALTER TABLE `parameter_pengukuran`
  MODIFY `id_parameter_pengukuran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pengukuran_air`
--
ALTER TABLE `pengukuran_air`
  MODIFY `id_pengukuran_air` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pengukuran_air_detail`
--
ALTER TABLE `pengukuran_air_detail`
  MODIFY `id_pengukuran_air_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pengukuran_udara`
--
ALTER TABLE `pengukuran_udara`
  MODIFY `id_pengukuran_udara` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pengukuran_udara_detail`
--
ALTER TABLE `pengukuran_udara_detail`
  MODIFY `id_pengukuran_udara_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `titik_air`
--
ALTER TABLE `titik_air`
  MODIFY `id_titik_air` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `titik_udara`
--
ALTER TABLE `titik_udara`
  MODIFY `id_titik_udara` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lokasi_air`
--
ALTER TABLE `lokasi_air`
  ADD CONSTRAINT `lokasi_air_ibfk_1` FOREIGN KEY (`id_jenis_air`) REFERENCES `jenis_air` (`id_jenis_air`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengukuran_air`
--
ALTER TABLE `pengukuran_air`
  ADD CONSTRAINT `pengukuran_air_ibfk_1` FOREIGN KEY (`id_titik_air`) REFERENCES `titik_air` (`id_titik_air`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengukuran_air_detail`
--
ALTER TABLE `pengukuran_air_detail`
  ADD CONSTRAINT `pengukuran_air_detail_ibfk_1` FOREIGN KEY (`id_pengukuran_air`) REFERENCES `pengukuran_air` (`id_pengukuran_air`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengukuran_air_detail_ibfk_2` FOREIGN KEY (`id_parameter_pengukuran`) REFERENCES `parameter_pengukuran` (`id_parameter_pengukuran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengukuran_udara_detail`
--
ALTER TABLE `pengukuran_udara_detail`
  ADD CONSTRAINT `pengukuran_udara_detail_ibfk_1` FOREIGN KEY (`id_pengukuran_udara`) REFERENCES `pengukuran_udara` (`id_pengukuran_udara`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengukuran_udara_detail_ibfk_2` FOREIGN KEY (`id_parameter_pengukuran`) REFERENCES `parameter_pengukuran` (`id_parameter_pengukuran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `titik_air`
--
ALTER TABLE `titik_air`
  ADD CONSTRAINT `titik_air_ibfk_1` FOREIGN KEY (`id_lokasi_air`) REFERENCES `lokasi_air` (`id_lokasi_air`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `titik_udara`
--
ALTER TABLE `titik_udara`
  ADD CONSTRAINT `titik_udara_ibfk_1` FOREIGN KEY (`id_lokasi_udara`) REFERENCES `lokasi_udara` (`id_lokasi_udara`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
