<?php $this->load->view('layout/header') ?>
	<section class="content-header">
		<h1>Data Titik Pantau</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<a href="<?= base_url('air/parameter_air/tambah') ?>">
							<button class="btn btn-primary">
								Tambah Data
							</button>
						</a>
					</div>
					<div class="box-body">
						<!--  -->
						<table class="table table-hover" id="table">
							<thead>
								<tr>
									<th>No.</th>
									<th>Parameter</th>
									<th>Baku Mutu</th>
									<th>Satuan</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data as $key => $value): ?>
									<tr>
										<td><?= $key+1; ?></td>
										<td><?= $value->parameter_pengukuran ?></td>
										<td><?= $value->baku_mutu ?></td>
										<td><?= $value->satuan_ukur ?></td>
										<td>
											<a href="<?= base_url('air/parameter_air/edit/'.$value->id_parameter_pengukuran) ?>">
												<button class="btn btn-warning">
													Edit
												</button>
											</a>
											<a href="<?= base_url('air/parameter_air/delete/'.$value->id_parameter_pengukuran) ?>" onclick="return confirm('Yakin Hapus ?');">
												<button class="btn btn-danger">
													Delete
												</button>
											</a>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php $this->load->view('layout/footer') ?>