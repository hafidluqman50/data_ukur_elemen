<?php $this->load->view('layout/header') ?>
	<section class="content-header">
		<h1>Data Jenis Air</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header">
						<a href="<?= base_url() ?>air/data_air/tambah">
							<button class="btn btn-primary" type="button">
								Tambah Data
							</button>
						</a>
					</div>
					<div class="box-body">
						<table class="table table-hover" width="100%" id="table">
							<thead>
								<tr>
									<th>No.</th>
									<th>Nama Jenis Air</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data as $key => $value): ?>
									<tr>
										<td><?= $key+1 ?></td>
										<td><?= $value->nama_jenis_air ?></td>
										<td>
											<a href="<?= base_url('air/data_air/edit/'.$value->id_jenis_air) ?>">
												<button class="btn btn-warning">Edit</button>
											</a>
											<a href="<?= base_url('air/data_air/detail/'.$value->id_jenis_air) ?>">
												<button class="btn btn-info">Detail</button>
											</a>
											<a href="<?= base_url('air/data_air/edit/'.$value->id_jenis_air) ?>" onclick="return confirm('Yakin Hapus ?')">
												<button class="btn btn-danger">Delete</button>
											</a>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php $this->load->view('layout/footer') ?>
<!-- <script>
$(document).ready(function() {
    var data_jenis_air = $('#data-air').DataTable({ 
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": '<?php echo site_url('datatables/data_jenis_air'); ?>',
            "type": "POST"
        },
        "columns": [
        	{"data": 0,width:100},
            {"data": 1,width:100},
            {"data": 2,width:100}
        ],
    });
    data_jenis_air.on('order.dt search.dt',function () {
        data_jenis_air.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        });
    }).draw();
});
</script> -->