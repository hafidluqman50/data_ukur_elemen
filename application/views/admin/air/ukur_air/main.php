<?php $this->load->view('layout/header') ?>
	<section class="content-header">
		<h1>Data Ukur Air</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<a href="<?= base_url('air/data_air/detail/'.$id.'/lokasi_pantau/'.$id_lokasi) ?>">
							<button class="btn btn-default">
								<span class="fa fa-arrow-left"></span> Kembali
							</button>
						</a>
						<a href="<?= base_url('air/data_air/detail/'.$id.'/lokasi_pantau/'.$id_lokasi.'/titik_pantau/'.$id_titik.'/tambah') ?>">
							<button class="btn btn-primary">
								Tambah Data
							</button>
						</a>
					</div>
					<div class="box-body">
						<table class="table table-hover" id="table">
							<thead>
								<tr>
									<th>No.</th>
									<th>Nomor Uji</th>
									<th>Tahun</th>
									<th>Bulan</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data as $key => $value): ?>
									<tr>
										<td>
										<?= $key+1 ?></td>
										<td><?= $value->nomor_uji ?></td>
										<td><?= $value->tahun ?></td>
										<td><?= $value->bulan ?></td>
										<td>
											<a href="<?= base_url('air/data_air/detail/'.$id.'/lokasi_pantau/'.$id_lokasi.'/titik_pantau/'.$id_titik.'/data_ukur/'.$value->id_pengukuran_air) ?>">
												<button class="btn btn-info">
													Detail
												</button>
											</a>
											<!-- <a href="<?= base_url('air/data_air/detail/'.$id.'/titik_pantau/'.$id_detail.'/lampiran/'.$value->id_pengukuran_air) ?>">
												<button class="btn btn-danger">
													Lampiran
												</button>
											</a> -->
											<a href="<?= base_url('air/data_air/detail/'.$id.'/lokasi_pantau/'.$id_lokasi.'/titik_pantau/'.$id_titik.'/delete/'.$value->id_pengukuran_air) ?>" onclick="return confirm('Yakin Hapus ?');">
												<button class="btn btn-danger">
													Delete
												</button>
											</a>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php $this->load->view('layout/footer') ?>