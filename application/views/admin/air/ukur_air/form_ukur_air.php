<?php $this->load->view('layout/header') ?>
	<section class="content-header">
		<h1>Form Titik Pantau</h1>
	</section>
	<section class="content">
		<form action="<?= base_url('air/data_air/detail/'.$id.'/lokasi_pantau/'.$id_lokasi.'/titik_pantau/'.$id_titik.'/save') ?>" method="POST">
			<div class="row">
				<div class="col-md-4">
					<div class="box box-default">
						<div class="box-header">
							<a href="<?= base_url('air/data_air/detail/'.$id.'/lokasi_pantau/'.$id_lokasi.'/titik_pantau/'.$id_titik) ?>">
								<button class="btn btn-default" type="button">
									<span class="fa fa-arrow-left"></span> Kembali
								</button>
							</a>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label for="">Nomor Uji</label>
								<input type="text" name="nomor_uji" class="form-control" value="<?= isset($row) ? $row->nomor_uji : '' ?>" placeholder="Isi Nomor Uji" required="required">
							</div>
							<div class="form-group">
								<label for="">Tahun</label>
								<input type="text" name="tahun" class="form-control" value="<?= isset($row) ? $row->tahun : '' ?>" placeholder="Isi Tahun" required="required">
							</div>
							<div class="form-group">
								<label for="">Bulan</label>
								<input type="text" name="bulan" class="form-control" value="<?= isset($row) ? $row->bulan : '' ?>" placeholder="Isi Bulan" required="required">
							</div>
						</div>
						<input type="hidden" name="id_detail" value="<?= $id ?>">
						<input type="hidden" name="id_lokasi" value="<?= $id_lokasi ?>">
						<input type="hidden" name="id_titik_air" value="<?= $id_titik ?>">
						<div class="box-footer">
							<button class="btn btn-primary">Simpan <span class="fa fa-save"></span></button>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="box box-default">
						<div class="box-header with-border">
							<button class="btn btn-success tambah-input" type="button">
								Tambah Input
							</button>
							<button class="btn btn-danger hapus-input" type="button" style="display:none;">
								Hapus Input
							</button>
						</div>
						<div class="box-body" id="mantap-gan">
							<div class="hasil-uji-input" id="hasil-uji-input">
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Parameter</label>
										<select name="parameter_ukur[]" class="form-control select2 parameter_ukur" required="required">
											<option value="" selected="selected" disabled="disabled">=== Pilih Parameter Ukur ===</option>
											<?php foreach ($parameter_ukur as $key => $value): ?>
											<option value="<?= $value->id_parameter_pengukuran ?>">
												<?= $value->parameter_pengukuran ?> | <?= $value->satuan_ukur ?>
											</option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Hasil Uji</label>
										<input type="text" name="hasil_uji[]" class="form-control" required="required" placeholder="Isi Hasil Uji">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</section>
<?php $this->load->view('layout/footer') ?>