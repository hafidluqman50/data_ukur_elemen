<?php $this->load->view('layout/header') ?>
	<section class="content-header">
		<h1>Form Titik Pantau</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<form action="<?= base_url('air/data_air/detail/'.$id.'/lampiran/'.$id_detail.'/proses') ?>" method="POST" enctype="multipart/form-data">
						<div class="box-header">
							<a href="<?= base_url('air/data_air/detail/'.$id) ?>">
								<button class="btn btn-default" type="button">
									<span class="fa fa-arrow-left"></span> Kembali
								</button>
							</a>
							<a href="<?= base_url('air/data_air/detail/'.$id.'/lampiran/'.$id_detail.'/download') ?>">
								<button class="btn btn-danger">
									Download Lampiran
								</button>
							</a>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label for="">Lampiran(PDF)</label>
								<input type="file" name="lampiran" class="form-control" required="required">
							</div>
						</div>
						<input type="hidden" name="id_titik_air" value="<?= $id_detail ?>">
						<div class="box-footer">
							<button class="btn btn-primary">Upload Lampiran <span class="fa fa-save"></span></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
<?php $this->load->view('layout/footer') ?>