<?php $this->load->view('layout/header') ?>
	<section class="content-header">
		<h1>Data Titik Pantau</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<a href="<?= base_url('udara/data_udara') ?>">
							<button class="btn btn-default" type="button">
								<span class="fa fa-arrow-left"></span> Kembali
							</button>
						</a>
						<a href="<?= base_url('udara/data_udara/detail/'.$id.'/tambah') ?>">
							<button class="btn btn-primary">
								Tambah Data
							</button>
						</a>
					</div>
					<div class="box-body">
						<table class="table table-hover" id="table">
							<thead>
								<tr>
									<th>No.</th>
									<th>Nama Titik</th>
									<th>Titik Pantau</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data as $key => $value): ?>
									<tr>
										<td><?= $key+1; ?></td>
										<td><?= $value->nama_titik ?></td>
										<td><?= $value->letak_titik ?></td>
										<td>
											<a href="<?= base_url('udara/data_udara/detail/'.$id.'/edit/'.$value->id_titik_udara) ?>">
												<button class="btn btn-warning">
													Edit
												</button>
											</a>
											<a href="<?= base_url('udara/data_udara/detail/'.$id.'/titik_pantau/'.$value->id_titik_udara) ?>">
												<button class="btn btn-info">
													Data Ukur
												</button>
											</a>
											<!-- <a href="<?= base_url('udara/data_udara/detail/'.$id.'/lampiran/'.$value->id_titik_udara) ?>">
												<button class="btn btn-danger">
													Lampiran
												</button>
											</a> -->
											<a href="<?= base_url('udara/data_udara/detail/'.$id.'/delete/'.$value->id_titik_udara) ?>" onclick="return confirm('Yakin Hapus ?');">
												<button class="btn btn-danger">
													Delete
												</button>
											</a>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php $this->load->view('layout/footer') ?>