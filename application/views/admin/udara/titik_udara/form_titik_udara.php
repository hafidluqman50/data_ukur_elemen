<?php $this->load->view('layout/header') ?>
	<section class="content-header">
		<h1>Form Titik Pantau</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<form action="<?= base_url('udara/data_udara/detail/'.$id.'/save') ?>" method="POST">
						<div class="box-header">
							<a href="<?= base_url('udara/data_udara/detail/'.$id) ?>">
								<button class="btn btn-default" type="button">
									<span class="fa fa-arrow-left"></span> Kembali
								</button>
							</a>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label for="">Nama Titik</label>
								<input type="text" name="nama_titik" class="form-control" value="<?= isset($row) ? $row->nama_titik : '' ?>" placeholder="Isi Titik Pantau" required="required">
							</div>
							<div class="form-group">
								<label for="">Titik Pantau</label>
								<input type="text" name="letak_titik" class="form-control" value="<?= isset($row) ? $row->letak_titik : '' ?>" placeholder="Isi Titik Pantau" required="required">
							</div>
						</div>
						<input type="hidden" name="id_lokasi_udara" value="<?= $id ?>">
						<input type="hidden" name="id_titik_udara" value="<?= isset($row) ? $row->id_titik_udara : '' ?>">
						<div class="box-footer">
							<button class="btn btn-primary">Simpan <span class="fa fa-save"></span></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
<?php $this->load->view('layout/footer') ?>