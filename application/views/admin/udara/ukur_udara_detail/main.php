<?php $this->load->view('layout/header') ?>
	<section class="content-header">
		<h1>Form Titik Pantau</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-4">
				<div class="box box-default">
					<div class="box-header">
						<a href="<?= base_url('udara/data_udara/detail/'.$id.'/titik_pantau/'.$id_titik) ?>">
							<button class="btn btn-default" type="button">
								<span class="fa fa-arrow-left"></span> Kembali
							</button>
						</a>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="">Nomor Uji</label>
							<input type="text" class="form-control" value="<?= isset($data_ukur) ? $data_ukur->nomor_uji : '' ?>" disabled="disabled">
						</div>
						<div class="form-group">
							<label for="">Tahun</label>
							<input type="text" class="form-control" value="<?= isset($data_ukur) ? $data_ukur->tahun : '' ?>" disabled="disabled">
						</div>
						<div class="form-group">
							<label for="">Bulan</label>
							<input type="text" class="form-control" value="<?= isset($data_ukur) ? $data_ukur->bulan : '' ?>" disabled="disabled">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="box box-default">
					<div class="box-body" id="mantap-gan">
						<?php foreach ($data_ukur_detail as $key => $value): ?>
						<div class="hasil-uji-input" id="hasil-uji-input">
							<div class="col-md-3">
								<div class="form-group">
									<label for="">Parameter</label>
									<input type="text" class="form-control" value="<?= $value->parameter_pengukuran ?>" disabled="disabled">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="">Satuan Ukur</label>
									<input type="text" class="form-control" value="<?= $value->satuan_ukur ?>" disabled="disabled">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="">Baku Mutu</label>
									<input type="text" class="form-control" value="<?= $value->baku_mutu ?>" disabled="disabled">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="">Hasil Uji</label>
									<input type="text" name="hasil_uji[]" class="form-control" value="<?= $value->hasil_uji; ?>" disabled="disabled">
								</div>
							</div>
						</div>
						<?php endforeach ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php $this->load->view('layout/footer') ?>