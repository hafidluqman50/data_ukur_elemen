<?php $this->load->view('layout/header') ?>
	<section class="content-header">
		<h1>Jenis Air</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<form action="<?= base_url() ?>udara/parameter_udara/save" method="POST">
						<div class="box-header">
							<a href="<?= base_url() ?>udara/parameter_udara">
								<button class="btn btn-default" type="button">
									<span class="fa fa-arrow-left"></span> Kembali
								</button>
							</a>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label for="">Parameter</label>
								<input type="text" name="parameter" class="form-control" value="<?= isset($row) ? $row->parameter_pengukuran : '' ?>" placeholder="Isi Parameter" required="required">
							</div>
							<div class="form-group">
								<label for="">Satuan</label>
								<input type="text" name="satuan" class="form-control" value="<?= isset($row) ? $row->satuan_ukur : '' ?>" placeholder="Isi Satuan; Ex: mg/l" required="required">
							</div>
							<div class="form-group">
								<label for="">Baku Mutu</label>
								<input type="text" name="baku_mutu" class="form-control" value="<?= isset($row) ? $row->baku_mutu : '' ?>" placeholder="Isi Baku Mutu" required="required">
							</div>
						</div>
						<input type="hidden" name="id_parameter_pengukuran" value="<?= isset($row) ? $row->id_parameter_pengukuran : '' ?>">
						<div class="box-footer">
							<button class="btn btn-primary">Simpan <span class="fa fa-save"></span></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
<?php $this->load->view('layout/footer') ?>