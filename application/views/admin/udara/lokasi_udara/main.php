<?php $this->load->view('layout/header'); ?>
	<section class="content-header">
		<h1>Data Lokasi Udara</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<a href="<?= base_url('udara/data_udara/tambah') ?>">
							<button class="btn btn-primary">
								Tambah Data
							</button>
						</a>
					</div>
					<div class="box-body">
						<table class="table table-hover" id="table">
							<thead>
								<tr>
									<th>No.</th>
									<th>Lokasi</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data as $key => $value): ?>
									<tr>
										<td><?= $key+1 ?></td>
										<td><?= $value->nama_lokasi ?></td>
										<td><a href="<?= base_url('udara/data_udara/edit/'.$value->id_lokasi_udara) ?>">
											<button class="btn btn-warning">
												Edit
											</button>
										</a><a href="<?= base_url('udara/data_udara/detail/'.$value->id_lokasi_udara) ?>">
											<button class="btn btn-info">
												Lihat Titik
											</button>
										</a><a href="<?= base_url('udara/data_udara/delete/'.$value->id_lokasi_udara) ?>" onclick="return confirm('Yakin Hapus ?');">
											<button class="btn btn-danger">
												Delete
											</button>
										</a></td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php $this->load->view('layout/footer'); ?>