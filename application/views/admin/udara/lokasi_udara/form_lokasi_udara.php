<?php $this->load->view('layout/header') ?>
	<section class="content-header">
		<h1>Form Lokasi Air</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<form action="<?= base_url('udara/data_udara/save') ?>" method="POST">
						<div class="box-header">
							<a href="<?= base_url('udara/data_udara') ?>">
								<button class="btn btn-default" type="button">
									<span class="fa fa-arrow-left"></span> Kembali
								</button>
							</a>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label for="">Lokasi Udara</label>
								<input type="text" name="nama_lokasi" value="<?= isset($row) ? $row->nama_lokasi : '' ?>" class="form-control" placeholder="Isi Lokasi Udara" required="required">
							</div>
						</div>
						<input type="hidden" name="id_lokasi_udara" value="<?= isset($row) ? $row->id_lokasi_udara : '' ?>">
						<div class="box-footer">
							<button class="btn btn-primary">Simpan <span class="fa fa-save"></span></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
<?php $this->load->view('layout/footer') ?>