		</div>
		<footer class="main-footer">
			<strong>Copyleft 2019</strong> Jupiter
		</footer>
	</div>
</body>
</html>
<!-- jQuery 3 -->
<script src="<?= base_url() ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url() ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- <script src="<?= base_url() ?>assets/bower_components/PACE/pace.min.js"></script> -->
<!-- FastClick -->
<script src="<?= base_url() ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>assets/dist/js/adminlte.min.js"></script>
<!-- SlimScroll -->
<script src="<?= base_url() ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?= base_url() ?>assets/dist/js/pages/dashboard2.js"></script> -->
<!-- <script src="<?= base_url() ?>assets/dist/js/demo.js"></script> -->
<script src="<?= base_url() ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/dist/js/custom.js"></script>
<script>
	$(function(){
		$('#table').DataTable();
	});
</script>

<script>
  $(function(){
  	$('.select2').select2({
  		width:'resolve'
  	});
  });
</script>