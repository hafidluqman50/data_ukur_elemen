<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pengukuran Elemen | <?= $title ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/pace/pace.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/select2/dist/css/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-green sidebar-mini fixed">
  <div class="wrappper">
    <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>U</b>EL</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Ukur</b> Elemen</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
          <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <img src="<?= base_url() ?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                          <span class="hidden-xs">User</span>
                        </a>
                        <ul class="dropdown-menu">
                          <!-- User image -->
                          <li class="user-header">
                            <img src="<?= base_url() ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                            <p>
                            	User
                            </p>
                          </li>
                          <li class="user-footer">
                            <div class="pull-left">
                              <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                              <a href="#" class="btn btn-default btn-flat">Logout</a>
                            </div>
                          </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
     <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?= base_url() ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>User</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li <?php if(isset($page)) echo $page == 'dashboard' ? 'class="active"' : '' ?>>
              <a href="<?= base_url() ?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview <?php if (isset($treeview)) echo $treeview == 'air' ? 'active menu-open' : '' ?>">
              <a href="#">
                <i class="fa fa-database"></i> <span>Air</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php if (isset($page)) echo $page == 'parameter-air' ? 'class="active"' : '' ?>>
                  <a href="<?= base_url() ?>air/parameter_air">
                      <i class="fa fa-circle-o"></i> <span>Parameter</span>
                  </a>
                </li>
                <li <?php if (isset($page)) echo $page == 'data-air' ? 'class="active"' : '' ?>>
                  <a href="<?= base_url() ?>air/data_air">
                      <i class="fa fa-circle-o"></i> <span>Data Air</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="treeview <?php if (isset($treeview)) echo $treeview == 'udara' ? 'active menu-open' : '' ?>">
              <a href="#">
                <i class="fa fa-database"></i> <span>Udara</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php if (isset($page)) echo $page == 'parameter-udara' ? 'class="active"' : '' ?>>
                  <a href="<?= base_url() ?>udara/parameter_udara">
                      <i class="fa fa-circle-o"></i> <span>Parameter</span>
                  </a>
                </li>
                <li <?php if (isset($page)) echo $page == 'data-udara' ? 'class="active"' : '' ?>>
                  <a href="<?= base_url() ?>udara/data_udara">
                      <i class="fa fa-circle-o"></i> <span>Data Udara</span>
                  </a>
                </li>
              </ul>
            </li>
            <!-- <li class="treeview <?php if (isset($treeview)) echo $treeview == 'tanah' ? 'active menu-open' : '' ?>">
              <a href="#">
                <i class="fa fa-database"></i> <span>Tanah</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php if (isset($page)) echo $page == 'parameter-tanah' ? 'class="active"' : '' ?>>
                  <a href="<?= base_url() ?>air/parameter_tanah">
                      <i class="fa fa-circle-o"></i> <span>Parameter</span>
                  </a>
                </li>
                <li <?php if (isset($page)) echo $page == 'data-tanah' ? 'class="active"' : '' ?>>
                  <a href="<?= base_url() ?>air/data_udara">
                      <i class="fa fa-circle-o"></i> <span>Data Udara</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="treeview <?php if (isset($treeview)) echo $treeview == 'sampah' ? 'active menu-open' : '' ?>">
              <a href="#">
                <i class="fa fa-database"></i> <span>Sampah</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php if (isset($page)) echo $page == 'parameter-sampah' ? 'class="active"' : '' ?>>
                  <a href="<?= base_url() ?>air/parameter_sampah">
                      <i class="fa fa-circle-o"></i> <span>Parameter</span>
                  </a>
                </li>
                <li <?php if (isset($page)) echo $page == 'data-sampah' ? 'class="active"' : '' ?>>
                  <a href="<?= base_url() ?>air/data_udara">
                      <i class="fa fa-circle-o"></i> <span>Data Sampah</span>
                  </a>
                </li>
              </ul>
            </li> -->
            </ul>
           </li>
          </ul>
        </section>
        <!-- /.sidebar -->
     </aside>
     <div class="content-wrapper">