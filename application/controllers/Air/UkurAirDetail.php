<?php 
defined('BASEPATH') or exit('Not Allowed');

class UkurAirDetail extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('UkurAirModel','ukur_air');
		$this->load->model('UkurAirDetailModel','ukur_air_detail');
	}

	public function index($id,$id_lokasi,$id_titik,$id_ukur)
	{
		$title            = 'Data Ukur Detail';
		$page             = 'data-air';
		$treeview         = 'air';
		$data_ukur        = $this->ukur_air->rowById($id_ukur);
		$data_ukur_detail = $this->ukur_air_detail->getData($id_ukur);

		$this->load->view('admin/air/ukur_air_detail/main',compact('title','page','treeview','data_ukur','data_ukur_detail','id','id_lokasi','id_titik','id_ukur'));
	}
}