<?php 
defined('BASEPATH') or exit('Not Allowed');

class TitikAir extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('TitikAirModel','titik_air');
	}

	public function index($id,$id_lokasi)
	{
		$title    = 'Data Titik Pantau';
		$page     = 'data-air';
		$treeview = 'air';
		$data     = $this->titik_air->getByIdJenis($id_lokasi);
		$this->load->view('admin/air/titik_air/main',compact('title','page','treeview','data','id','id_lokasi'));
	}

	public function tambah($id,$id_lokasi)
	{
		$title    = 'Form Titik Pantau';
		$page     = 'data-air';
		$treeview = 'air';
		$this->load->view('admin/air/titik_air/form_titik_air',compact('title','page','treeview','id','id_lokasi'));
	}

	public function edit($id,$id_lokasi,$id_titik)
	{
		$title    = 'Form Titik Pantau';
		$page     = 'data-air';
		$treeview = 'air';
		$row      = $this->titik_air->rowById($id_lokasi,$id_titik);
		$this->load->view('admin/air/titik_air/form_titik_air',compact('title','page','treeview','row','id','id_lokasi','id_titik'));
	}

	public function delete($id,$id_lokasi,$id_titik)
	{
		$this->titik_air->deleteData($id_lokasi,$id_titik);
		// $this->session->set_flashdata('message','Berhasil Hapus Data');
		redirect('air/data_air/detail/'.$id.'/lokasi_pantau/'.$id_lokasi);
	}

	public function save()
	{
		$nama_titik    = $this->input->post('nama_titik');
		$letak_titik   = $this->input->post('letak_titik');
		$id_jenis_air  = $this->input->post('id_jenis_air');
		$id_lokasi_air = $this->input->post('id_lokasi_air');
		$id_titik_air  = $this->input->post('id_titik_air');

		$data = [
			'id_lokasi_air' => $id_lokasi_air,
			'nama_titik'    => $nama_titik,
			'letak_titik'   => $letak_titik
		];

		if ($id_titik_air == '') {
			$this->titik_air->insertData($data);
		}
		else {
			$this->titik_air->updateData($id_lokasi_air,$id_titik_air,$data);
		}

		redirect('air/data_air/detail/'.$id_jenis_air.'/lokasi_pantau/'.$id_lokasi_air);
	}

	// public function lampiran($id,$id_detail)
	// {
	// 	$title    = 'Lampiran Titik Pantau';
	// 	$page     = 'data-air';
	// 	$treeview = 'air';
	// 	$this->load->view('admin/air/titik_air/form_lampiran',compact('title','page','treeview','id','id_detail'));
	// }

	// public function lampiranProses($id,$id_detail)
	// {
	// 	$lampiran = $this->input->post('lampiran');
		
	// 	$config = [
	// 		'file_name'   => $_FILES['lampiran']['name'],
	// 		'upload_path' => './assets/lampiran/lampiran_titik_pantau'
	// 	];

	// 	$this->load->library('upload',$config);
	// 	$this->upload->initialize($config);
		
	// 	if ($this->upload->do_upload('lampiran')) {
	// 		$data['dokumentasi'] = $_FILES['lampiran']['name'];
	// 		$this->titik_air->updateData($id_detail,$id,$data);
	// 		redirect('air/data_air/detail/'.$id.'/lampiran/'.$id_detail);
	// 	}
	// }
}