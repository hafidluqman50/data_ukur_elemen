<?php 
defined('BASEPATH') or exit('Not Allowed !!');

class JenisAir extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		// $this->load->library('session');
		$this->load->model('JenisAirModel','jenis_air');
	}

	public function index()
	{
		$title    = 'Jenis Air';
		$page     = 'data-air';
		$treeview = 'air';
		$data     = $this->jenis_air->getAll();
		$this->load->view('admin/air/jenis_air/main',compact('title','page','treeview','data'));
	}

	public function tambah()
	{
		$title    = 'Form Jenis Air';
		$page     = 'data-air';
		$treeview = 'air';
		$this->load->view('admin/air/jenis_air/form_jenis_air',compact('title','page','treeview'));
	}

	public function edit($id)
	{
		$title    = 'Form Jenis Air';
		$page     = 'data-air';
		$treeview = 'air';
		$row      = $this->jenis_air->rowById($id);
		$this->load->view('admin/air/jenis_air/form_jenis_air',compact('title','page','treeview','row'));
	}

	public function delete($id)
	{
		$this->jenis_air->deleteData($id);
		// $this->session->set_flashdata('message','Berhasil Menghapus Data');
		redirect('/air/jenis_air');
	}

	public function save()
	{
		$nama_jenis   = $this->input->post('keterangan_air');
		$id_jenis_air = $this->input->post('id_jenis_air');
		$data = ['nama_jenis_air' => $nama_jenis];

		if ($id_jenis_air == '') {
			$this->jenis_air->insertData($data);
		}
		else {
			$this->jenis_air->updateData($id_jenis_air,$data);
		}

		redirect('air/data_air');
	}
}