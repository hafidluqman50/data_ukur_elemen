<?php 
defined('BASEPATH') or exit('Not Allowed');

class ParameterUkurAir extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		// var_dump($this->session);
		$this->load->model('ParameterUkurModel','parameter_ukur');
	}

	public function index()
	{
		$title    = 'Parameter Air';
		$page     = 'parameter-air';
		$treeview = 'air';
		$data     = $this->parameter_ukur->getAll('air');
		$this->load->view('admin/air/parameter_air/main',compact('title','page','treeview','data'));
	}

	public function tambah()
	{
		$title    = 'Form Parameter Air';
		$page     = 'parameter-air';
		$treeview = 'air';
		$this->load->view('admin/air/parameter_air/form_parameter_air',compact('title','page','treeview'));
	}

	public function edit($id)
	{
		$title    = 'Form Parameter Air';
		$page     = 'parameter-air';
		$treeview = 'air';
		$row      = $this->parameter_ukur->rowById($id,'air');
		$this->load->view('admin/air/parameter_air/form_parameter_air',compact('title','page','treeview','row'));
	}

	public function delete($id)
	{
		$this->parameter_ukur->deleteData($id);
		// $this->session->set_flashdata('message','Berhasil Hapus Data');
		redirect('air/parameter_air');
	}

	public function save()
	{
		$nama_parameter          = $this->input->post('parameter');
		$satuan_ukur             = $this->input->post('satuan');
		$baku_mutu               = $this->input->post('baku_mutu');
		$id_parameter_pengukuran = $this->input->post('id_parameter_pengukuran');

		$data = [
			'parameter_pengukuran' => $nama_parameter,
			'satuan_ukur'          => $satuan_ukur,
			'baku_mutu'            => $baku_mutu,
			'ket_ukur'			   => 'air'
		];

		if ($id_parameter_pengukuran == '') {
			$this->parameter_ukur->insertData($data);
			// $this->session->set_flashdata('message','Berhasil Input Data');
		}
		else {
			$this->parameter_ukur->updateData($id_parameter_pengukuran,$data);
			// $this->session->set_flashdata('message','Berhasil Update Data');
		}

		redirect('air/parameter_air');
	}
}