<?php 
defined('BASEPATH') or exit('Not Allowed');

class LokasiAir extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('LokasiAirModel','lokasi_air');
	}

	public function index($id)
	{
		$title    = 'Lokasi Air';
		$page     = 'data-air';
		$treeview = 'air';
		$data     = $this->lokasi_air->getAll($id);
		$this->load->view('admin/air/lokasi_air/main',compact('title','page','treeview','data','id'));
	}

	public function tambah($id)
	{
		$title    = 'Form Lokasi Air';
		$page     = 'data-air';
		$treeview = 'air';
		$this->load->view('admin/air/lokasi_air/form_lokasi_air',compact('title','page','treeview','id'));
	}

	public function edit($id,$id_lokasi)
	{
		$title    = 'Form Lokasi Air';
		$page     = 'data-air';
		$treeview = 'air';
		$row = $this->lokasi_air->rowById($id,$id_lokasi);
		$this->load->view('admin/air/lokasi_air/form_lokasi_air',compact('title','page','treeview','row','id'));
	}

	public function delete($id,$id_lokasi)
	{
		$this->lokasi_air->deleteData($id,$id_lokasi);
		redirect('air/data_air/detail/'.$id);
	}

	public function save()
	{
		$nama_lokasi   = $this->input->post('nama_lokasi');
		$id_jenis_air  = $this->input->post('id_jenis_air');
		$id_lokasi_air = $this->input->post('id_lokasi_air');

		$data_lokasi = [
			'id_jenis_air' => $id_jenis_air,
			'nama_lokasi'   => $nama_lokasi
		];

		if ($id_lokasi_air == '') {
			$this->lokasi_air->insertData($data_lokasi);
		}
		else {
			$this->lokasi_air->updateData($id,$id_lokasi,$data_lokasi);
		}

		redirect('air/data_air/detail/'.$id_jenis_air);
	}
}