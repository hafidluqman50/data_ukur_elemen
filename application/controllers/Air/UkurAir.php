<?php 
defined('BASEPATH') or exit('Not Allowed!!!');

class UkurAir extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('UkurAirModel','ukur_air');
		$this->load->model('UkurAirDetailModel','ukur_air_detail');
		$this->load->model('ParameterUkurModel','parameter_ukur');
	}

	public function index($id,$id_lokasi,$id_titik)
	{
		$title    = 'Data Ukur Air';
		$page     = 'data-air';
		$treeview = 'air';
		$data     = $this->ukur_air->getAll($id_titik);
		$this->load->view('admin/air/ukur_air/main',compact('title','page','treeview','data','id','id_lokasi','id_titik'));
	}

	public function tambah($id,$id_lokasi,$id_titik)
	{
		$title          = 'Data Ukur Air';
		$page           = 'data-air';
		$treeview       = 'air';
		$parameter_ukur = $this->parameter_ukur->getAll('air');
		$this->load->view('admin/air/ukur_air/form_ukur_air',compact('title','page','treeview','id','id_lokasi','id_titik','parameter_ukur'));
	}

	public function edit()
	{

	}

	public function delete($id,$id_lokasi,$id_titik,$id_ukur)
	{
		$this->ukur_air->deleteData($id_titik,$id_ukur);

		redirect('air/data_air/detail/'.$id.'/lokasi_pantau/'.$id_lokasi.'/titik_pantau/'.$id_titik);
	}

	public function save()
	{
		$id_titik_air = $this->input->post('id_titik_air');
		$id_detail	  = $this->input->post('id_detail');
		$id_lokasi    = $this->input->post('id_lokasi');
		$nomor_uji    = $this->input->post('nomor_uji');
		// $lokasi       = $this->input->post('lokasi');
		$tahun        = $this->input->post('tahun');
		$bulan		  = $this->input->post('bulan');
		$parameter    = $this->input->post('parameter_ukur');
		$hasil_uji    = $this->input->post('hasil_uji');

		$data_ukur = [
			'id_titik_air' => $id_titik_air,
			'nomor_uji'    => $nomor_uji,
			// 'lokasi'       => $lokasi,
			'tahun'        => $tahun,
			'bulan'		   => $bulan,
		];

		$id_pengukuran_air = $this->ukur_air->insertGetId($data_ukur);

		for ($i=0; $i < count($parameter); $i++) { 
			$data_hasil[] = [
				'id_pengukuran_air'       => $id_pengukuran_air,
				'id_parameter_pengukuran' => $parameter[$i],
				'hasil_uji'               => $hasil_uji[$i]
			];
		}

		$this->ukur_air_detail->insertMass($data_hasil);

		redirect('air/data_air/detail/'.$id_detail.'/lokasi_pantau/'.$id_lokasi.'/titik_pantau/'.$id_titik_air.'/data_ukur/'.$id_pengukuran_air);
	}

	// public function lampiran($id,$id_titik,$id_ukur)
	// {
	// 	$title = 'Form Lampiran Data Ukur';
	// 	$page = 'data-air';
	// 	$treeview = 'air';
	// 	$this->load->view('admin/air/ukur_air/form_lampiran',compact('title','page','treeview','id','id_titik','id_ukur'));
	// }
}