<?php 
defined('BASEPATH') or exit('Not Allowed!!!');

class UkurUdara extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('UkurUdaraModel','ukur_udara');
		$this->load->model('UkurUdaraDetailModel','ukur_udara_detail');
		$this->load->model('ParameterUkurModel','parameter_ukur');
	}

	public function index($id,$id_titik)
	{
		$title    = 'Data Ukur Udara';
		$page     = 'data-udara';
		$treeview = 'udara';
		$data     = $this->ukur_udara->getAll($id_titik);
		$this->load->view('admin/udara/ukur_udara/main',compact('title','page','treeview','data','id','id_titik'));
	}

	public function tambah($id,$id_titik)
	{
		$title          = 'Data Ukur Udara';
		$page           = 'data-udara';
		$treeview       = 'udara';
		$parameter_ukur = $this->parameter_ukur->getAll('udara');
		$this->load->view('admin/udara/ukur_udara/form_ukur_udara',compact('title','page','treeview','id','id_titik','parameter_ukur'));
	}

	public function edit()
	{

	}

	public function delete($id,$id_titik,$id_ukur)
	{
		$this->ukur_udara->deleteData($id_titik,$id_ukur);

		redirect('udara/data_udara/detail/'.$id.'/titik_pantau/'.$id_titik);
	}

	public function save()
	{
		$id_titik_udara = $this->input->post('id_titik_udara');
		$id_detail      = $this->input->post('id_detail');
		$nomor_uji      = $this->input->post('nomor_uji');
		// $lokasi      = $this->input->post('lokasi');
		$tahun          = $this->input->post('tahun');
		$bulan          = $this->input->post('bulan');
		$parameter      = $this->input->post('parameter_ukur');
		$hasil_uji      = $this->input->post('hasil_uji');

		$data_ukur = [
			'id_titik_udara' => $id_titik_udara,
			'nomor_uji'      => $nomor_uji,
			'tahun'          => $tahun,
			'bulan'          => $bulan,
		];

		$id_pengukuran_udara = $this->ukur_udara->insertGetId($data_ukur);

		for ($i=0; $i < count($parameter); $i++) { 
			$data_hasil[] = [
				'id_pengukuran_udara'     => $id_pengukuran_udara,
				'id_parameter_pengukuran' => $parameter[$i],
				'hasil_uji'               => $hasil_uji[$i]
			];
		}

		$this->ukur_udara_detail->insertMass($data_hasil);

		redirect('udara/data_udara/detail/'.$id_detail.'/titik_pantau/'.$id_titik_udara.'/data_ukur/'.$id_pengukuran_udara);
	}

	// public function lampiran($id,$id_titik,$id_ukur)
	// {
	// 	$title = 'Form Lampiran Data Ukur';
	// 	$page = 'data-udara';
	// 	$treeview = 'udara';
	// 	$this->load->view('admin/udara/ukur_udara/form_lampiran',compact('title','page','treeview','id','id_titik','id_ukur'));
	// }
}