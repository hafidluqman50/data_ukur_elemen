<?php 
defined('BASEPATH') or exit('Not Allowed');

class LokasiUdara extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('LokasiUdaraModel','lokasi_udara');
	}

	public function index()
	{
		$title    = 'Lokasi Air';
		$page     = 'data-udara';
		$treeview = 'udara';
		$data     = $this->lokasi_udara->getAll();
		$this->load->view('admin/udara/lokasi_udara/main',compact('title','page','treeview','data'));
	}

	public function tambah()
	{
		$title    = 'Form Lokasi Air';
		$page     = 'data-udara';
		$treeview = 'udara';
		$this->load->view('admin/udara/lokasi_udara/form_lokasi_udara',compact('title','page','treeview'));
	}

	public function edit($id)
	{
		$title    = 'Form Lokasi Air';
		$page     = 'data-udara';
		$treeview = 'udara';
		$row = $this->lokasi_udara->rowById($id);
		$this->load->view('admin/udara/lokasi_udara/form_lokasi_udara',compact('title','page','treeview','row','id'));
	}

	public function delete($id)
	{
		$this->lokasi_udara->deleteData($id);
		redirect('udara/data_udara');
	}

	public function save()
	{
		$nama_lokasi     = $this->input->post('nama_lokasi');
		$id_lokasi_udara = $this->input->post('id_lokasi_udara');

		$data_lokasi = [
			'nama_lokasi'   => $nama_lokasi
		];

		if ($id_lokasi_udara == '') {
			$this->lokasi_udara->insertData($data_lokasi);
		}
		else {
			$this->lokasi_udara->updateData($id_lokasi_udara,$data_lokasi);
		}

		redirect('udara/data_udara');
	}
}