<?php 
defined('BASEPATH') or exit('Not Allowed');

class TitikUdara extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('TitikUdaraModel','titik_udara');
	}

	public function index($id)
	{
		$title    = 'Data Titik Pantau';
		$page     = 'data-udara';
		$treeview = 'udara';
		$data     = $this->titik_udara->getByIdJenis($id);
		$this->load->view('admin/udara/titik_udara/main',compact('title','page','treeview','data','id'));
	}

	public function tambah($id)
	{
		$title    = 'Form Titik Pantau';
		$page     = 'data-udara';
		$treeview = 'udara';
		$this->load->view('admin/udara/titik_udara/form_titik_udara',compact('title','page','treeview','id'));
	}

	public function edit($id,$id_titik)
	{
		$title    = 'Form Titik Pantau';
		$page     = 'data-udara';
		$treeview = 'udara';
		$row      = $this->titik_udara->rowById($id,$id_titik);
		$this->load->view('admin/udara/titik_udara/form_titik_udara',compact('title','page','treeview','row','id','id_titik'));
	}

	public function delete($id,$id_titik)
	{
		$this->titik_udara->deleteData($id,$id_titik);
		// $this->session->set_flashdata('message','Berhasil Hapus Data');
		redirect('udara/data_udara/detail/'.$id);
	}

	public function save()
	{
		$nama_titik      = $this->input->post('nama_titik');
		$letak_titik     = $this->input->post('letak_titik');
		$id_lokasi_udara = $this->input->post('id_lokasi_udara');
		$id_titik_udara  = $this->input->post('id_titik_udara');

		$data = [
			'id_lokasi_udara' => $id_lokasi_udara,
			'nama_titik'      => $nama_titik,
			'letak_titik'     => $letak_titik
		];

		if ($id_titik_udara == '') {
			$this->titik_udara->insertData($data);
		}
		else {
			$this->titik_udara->updateData($id_lokasi_udara,$id_titik_udara,$data);
		}

		redirect('udara/data_udara/detail/'.$id_lokasi_udara);
	}

	// public function lampiran($id,$id_detail)
	// {
	// 	$title    = 'Lampiran Titik Pantau';
	// 	$page     = 'data-udara';
	// 	$treeview = 'udara';
	// 	$this->load->view('admin/udara/titik_udara/form_lampiran',compact('title','page','treeview','id','id_detail'));
	// }

	// public function lampiranProses($id,$id_detail)
	// {
	// 	$lampiran = $this->input->post('lampiran');
		
	// 	$config = [
	// 		'file_name'   => $_FILES['lampiran']['name'],
	// 		'upload_path' => './assets/lampiran/lampiran_titik_pantau'
	// 	];

	// 	$this->load->library('upload',$config);
	// 	$this->upload->initialize($config);
		
	// 	if ($this->upload->do_upload('lampiran')) {
	// 		$data['dokumentasi'] = $_FILES['lampiran']['name'];
	// 		$this->titik_udara->updateData($id_detail,$id,$data);
	// 		redirect('udara/data_udara/detail/'.$id.'/lampiran/'.$id_detail);
	// 	}
	// }
}