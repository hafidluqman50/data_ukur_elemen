<?php 
defined('BASEPATH') or exit('Not Allowed');

class UkurUdaraDetail extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('UkurUdaraModel','ukur_udara');
		$this->load->model('UkurUdaraDetailModel','ukur_udara_detail');
	}

	public function index($id,$id_titik,$id_ukur)
	{
		$title            = 'Data Ukur Detail';
		$page             = 'data-udara';
		$treeview         = 'udara';
		$data_ukur        = $this->ukur_udara->rowById($id_ukur);
		$data_ukur_detail = $this->ukur_udara_detail->getData($id_ukur);

		$this->load->view('admin/udara/ukur_udara_detail/main',compact('title','page','treeview','data_ukur','data_ukur_detail','id','id_titik','id_ukur'));
	}
}