<?php 
defined('BASEPATH') or exit('Not Allowed');

class ParameterUkurUdara extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		// var_dump($this->session);
		$this->load->model('ParameterUkurModel','parameter_ukur');
	}

	public function index()
	{
		$title    = 'Parameter Udara';
		$page     = 'parameter-udara';
		$treeview = 'udara';
		$data     = $this->parameter_ukur->getAll('udara');
		$this->load->view('admin/udara/parameter_udara/main',compact('title','page','treeview','data'));
	}

	public function tambah()
	{
		$title    = 'Form Parameter Udara';
		$page     = 'parameter-udara';
		$treeview = 'udara';
		$this->load->view('admin/udara/parameter_udara/form_parameter_udara',compact('title','page','treeview'));
	}

	public function edit($id)
	{
		$title    = 'Form Parameter Udara';
		$page     = 'parameter-udara';
		$treeview = 'udara';
		$row      = $this->parameter_ukur->rowById($id,'udara');
		$this->load->view('admin/udara/parameter_udara/form_parameter_udara',compact('title','page','treeview','row'));
	}

	public function delete($id)
	{
		$this->parameter_ukur->deleteData($id);
		// $this->session->set_flashdata('message','Berhasil Hapus Data');
		redirect('udara/parameter_udara');
	}

	public function save()
	{
		$nama_parameter          = $this->input->post('parameter');
		$satuan_ukur             = $this->input->post('satuan');
		$baku_mutu               = $this->input->post('baku_mutu');
		$id_parameter_pengukuran = $this->input->post('id_parameter_pengukuran');

		$data = [
			'parameter_pengukuran' => $nama_parameter,
			'satuan_ukur'          => $satuan_ukur,
			'baku_mutu'            => $baku_mutu,
			'ket_ukur'			   => 'udara',
			'status_data'		   => '1'
		];

		if ($id_parameter_pengukuran == '') {
			$this->parameter_ukur->insertData($data);
			// $this->session->set_flashdata('message','Berhasil Input Data');
		}
		else {
			$this->parameter_ukur->updateData($id_parameter_pengukuran,$data);
			// $this->session->set_flashdata('message','Berhasil Update Data');
		}

		redirect('udara/parameter_udara');
	}
}