<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	public function index() 
	{
		$title = 'Dashboard';
		$page  = 'dashboard';
		$this->load->view('admin/dashboard',compact('title','page'));
	}
}