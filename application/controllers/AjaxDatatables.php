<?php 
defined('BASEPATH') or exit('Not Allowed');

class AjaxDatatables extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('datatables');
		$this->load->model('JenisAirModel','jenis_air');
	}

	public function dataJenisAir()
	{
		header('Content-Type: application/json');
		echo $this->jenis_air->getDatatables();
	}
}