<?php 
defined('BASEPATH') or exit('Not Allowed');

class ParameterUkurModel extends CI_Controller 
{
	protected $tableName = 'parameter_pengukuran';

	public function __construct()
	{
		parent::__construct();
	}

	public function getAll($ket)
	{
		$this->db->where('status_data','1');
		$this->db->where('ket_ukur',$ket);
		$query = $this->db->get($this->tableName);
		return $query->result_object();
	}

	public function rowById($id)
	{
		$this->db->where('id_parameter_pengukuran',$id);
		$query = $this->db->get($this->tableName);

		if ($query->num_rows() >= 1) {
			return $query->row_object();
		}
		else {
			return [];
		}
	}

	public function insertData(array $data)
	{
		$this->db->insert($this->tableName,$data);
	}

	public function updateData($id,array $data)
	{
		$this->db->where('id_parameter_pengukuran',$id);
		$this->db->update($this->tableName,$data);
	}

	public function deleteData($id)
	{
		$this->db->where('id_parameter_pengukuran',$id);
		$this->db->update($this->tableName,['status_data'=>'0']);
	}
}