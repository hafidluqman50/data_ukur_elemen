<?php 
defined('BASEPATH') or exit('Not Allowed');

class TitikAirModel extends CI_Controller
{
	protected $tableName = 'titik_air';

	public function __construct()
	{
		parent::__construct();
	}

	public function getByIdJenis($id)
	{
		$this->db->where('id_lokasi_air',$id);
		$this->db->select('*');
		$query = $this->db->get($this->tableName);
		return $query->result_object();
	}

	public function rowById($id,$id_detail)
	{
		$this->db->where('id_lokasi_air',$id_detail);
		$this->db->where('id_titik_air',$id);
		$query = $this->db->get($this->tableName);

		if ($query->num_rows() >= 1) {
			return $query->row_object();
		}
		else {
			return [];
		}
	}

	public function insertData(array $data)
	{
		$this->db->insert($this->tableName,$data);
	}

	public function updateData($id,$id_detail,array $data)
	{
		$this->db->where('id_lokasi_air',$id_detail);
		$this->db->where('id_titik_air',$id);
		$this->db->update($this->tableName,$data);
	}

	public function deleteData($id,$id_detail)
	{
		$this->db->where('id_lokasi_air',$id_detail);
		$this->db->where('id_titik_air',$id);
		$this->db->delete($this->tableName);
	}
}