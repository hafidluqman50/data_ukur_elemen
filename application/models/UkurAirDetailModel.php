<?php 
defined('BASEPATH') or exit('Not Allowed');

class UkurAirDetailModel extends CI_Model
{
	protected $tableName = 'pengukuran_air_detail';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getData($id)
	{
		$this->db->join('parameter_pengukuran','pengukuran_air_detail.id_parameter_pengukuran = parameter_pengukuran.id_parameter_pengukuran');
		$this->db->where('id_pengukuran_air',$id);
		$query = $this->db->get($this->tableName);
		return $query->result_object();
	}

	public function insertMass(array $data)
	{
		$this->db->insert_batch($this->tableName,$data);
	}
}