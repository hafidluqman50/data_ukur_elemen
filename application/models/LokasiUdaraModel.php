<?php 
defined('BASEPATH') or exit('Not Allowed');

class LokasiUdaraModel extends CI_Model {
	
	protected $tableName = 'lokasi_udara';

	public function __construct() 
	{
		parent::__construct();
	}

	public function getAll()
	{
		$query = $this->db->get($this->tableName);

		return $query->result_object();
	}

	public function rowById($id)
	{
		$this->db->where('id_lokasi_udara',$id);

		$query = $this->db->get($this->tableName);

		if ($query->num_rows() >= 1) {
			return $query->row_object();
		}
		else {
			return [];
		}
	}

	public function insertData(array $data)
	{
		$this->db->insert($this->tableName,$data);
	}

	public function updateData($id,array $data)
	{
		$this->db->where('id_lokasi_udara',$id);
		$this->db->update($this->tableName,$data);
	}

	public function deleteData($id)
	{
		$this->db->where('id_lokasi_udara',$id);
		$this->db->delete($this->tableName);
	}
}