<?php 
defined('BASEPATH') or exit('Not Allowed');

class JenisAirModel extends CI_Model
{
	protected $tableName = 'jenis_air';

	public function __construct()
	{
		parent::__construct();
	}

	public function getAll()
	{
		$query = $this->db->get($this->tableName);
		return $query->result_object();
	}

	public function rowById($id)
	{
		$this->db->where('id_jenis_air',$id);
		$query = $this->db->get($this->tableName);

		if ($query->num_rows() >= 1) {
			return $query->row_object();
		}
		else {
			return [];
		}
	}

	public function insertData(array $data)
	{
		$this->db->insert($this->tableName,$data);
	}

	public function updateData($id,array $data)
	{
		$this->db->where('id_jenis_air',$id);
		$this->db->update($this->tableName,$data);
	}

	public function deleteData($id)
	{
		$this->db->where('id_jenis_air',$id);
		$this->db->delete($this->tableName);
	}

	public function getDatatables()
	{
		$this->datatables->select('*');
		$this->datatables->from($this->tableName);
		$this->datatables->add_column('action','<a href="'.base_url().'air/data_air/.$1"><button class="btn btn-warning">Edit</button</a>');
		return $this->datatables->generate();
	}
}