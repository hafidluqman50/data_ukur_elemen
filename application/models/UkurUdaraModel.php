<?php 
defined('BASEPATH') or exit('Not Allowed');

class UkurUdaraModel extends CI_Model
{
	protected $tableName = 'pengukuran_udara';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getAll($id)
	{
		$this->db->where('id_titik_udara',$id);
		$query = $this->db->get($this->tableName);
		return $query->result_object();
	}

	public function rowById($id)
	{
		$this->db->where('id_pengukuran_udara',$id);
		$query = $this->db->get($this->tableName);

		if ($query->num_rows() >= 1) {
			return $query->row_object();
		}
		else {
			return [];
		}
	}

	public function insertData(array $data)
	{
		$this->db->insert($this->tableName,$data);
	}

	public function insertGetId(array $data)
	{
		$this->db->insert($this->tableName,$data);
		$get_id = $this->db->insert_id();

		return $get_id;	
	}

	// public function updateData($id,$id_detail,$id_titik,array $data)
	// {
	// 	$this->db->where('id_pengukuran_udara',$id);
	// 	$this->db->update($this->tableName,$data);
	// }

	public function deleteData($id,$id_titik)
	{
		$this->db->where('id_pengukuran_udara',$id);
		$this->db->where('id_titik_udara',$id_titik);
		$this->db->delete($this->tableName);
	}
}