<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// ROUTING PARAMETER AIR //
$route['air/parameter_air'] = 'Air/ParameterUkurAir/index';
$route['air/parameter_air/tambah'] = 'Air/ParameterUkurAir/tambah';
$route['air/parameter_air/edit/(:num)'] = 'Air/ParameterUkurAir/edit/$1';
$route['air/parameter_air/delete/(:num)'] = 'Air/ParameterUkurAir/delete/$1';
$route['air/parameter_air/save'] = 'Air/ParameterUkurAir/save';
// END ROUTING PARAMETER AIR //

// ROUTING JENIS AIR //
$route['air/data_air'] = 'Air/JenisAir/index';
$route['air/data_air/tambah'] = 'Air/JenisAir/tambah';
$route['air/data_air/edit/(:num)'] = 'Air/JenisAir/edit/$1';
$route['air/data_air/delete/(:num)'] = 'Air/JenisAir/delete/$1';
$route['air/data_air/save'] = 'Air/JenisAir/save';
// END ROUTING JENIS AIR //

// ROUTING LOKASI PANTAU //
$route['air/data_air/detail/(:num)'] = 'Air/LokasiAir/index/$1';
$route['air/data_air/detail/(:num)/tambah'] = 'Air/LokasiAir/tambah/$1';
$route['air/data_air/detail/(:num)/edit/(:num)'] = 'Air/LokasiAir/edit/$1/$2';
// $route['air/data_air/detail/(:num)/lampiran/(:num)'] = 'Air/LokasiAir/lampiran/$1/$2';
// $route['air/data_air/detail/(:num)/lampiran/(:num)/proses'] = 'Air/LokasiAir/lampiranProses/$1/$2';
// $route['air/data_air/detail/(:num)/lampiran/(:num)/download'] = 'Air/LokasiAir/lampiranDownload/$1/$2';
$route['air/data_air/detail/(:num)/delete/(:num)'] = 'Air/LokasiAir/delete/$1/$2';
$route['air/data_air/detail/(:num)/save'] = 'Air/LokasiAir/save';
// END ROUTING LOKASI PANTAU //

// ROUTING TITIK PANTAU //
$route['air/data_air/detail/(:num)/lokasi_pantau/(:num)'] = 'Air/TitikAir/index/$1/$2';
$route['air/data_air/detail/(:num)/lokasi_pantau/(:num)/tambah'] = 'Air/TitikAir/tambah/$1/$2';
$route['air/data_air/detail/(:num)/lokasi_pantau/(:num)/edit/(:num)'] = 'Air/TitikAir/edit/$1/$2/$3';
// $route['air/data_air/detail/(:num)/lokasi_pantau/(:num)/lampiran/(:num)'] = 'Air/TitikAir/lampiran/$1/$2/$3';
// $route['air/data_air/detail/(:num)/lokasi_pantau/(:num)/lampiran/(:num)/proses'] = 'Air/TitikAir/lampiranProses/$1/$2/$3';
// $route['air/data_air/detail/(:num)/lokasi_pantau/(:num)/lampiran/(:num)/download'] = 'Air/TitikAir/lampiranDownload/$1/$2/$3';
$route['air/data_air/detail/(:num)/lokasi_pantau/(:num)/delete/(:num)'] = 'Air/TitikAir/delete/$1/$2/$3';
$route['air/data_air/detail/(:num)/lokasi_pantau/(:num)/save'] = 'Air/TitikAir/save/$1/$2';
// END ROUTING TITIK PANTAU //

// ROUTING DATA UKUR AIR //
$route['air/data_air/detail/(:num)/lokasi_pantau/(:num)/titik_pantau/(:num)'] = 'Air/UkurAir/index/$1/$2/$3';
$route['air/data_air/detail/(:num)/lokasi_pantau/(:num)/titik_pantau/(:num)/tambah'] = 'Air/UkurAir/tambah/$1/$2/$3';
// $route['air/data_air/detail/(:num)/lokasi_pantau/(:num)/titik_pantau/(:num)'] = 'Air/UkurAir/index/$1/$2/$3';
$route['air/data_air/detail/(:num)/lokasi_pantau/(:num)/titik_pantau/(:num)/delete/(:num)'] = 'Air/UkurAir/delete/$1/$2/$3/$4';
$route['air/data_air/detail/(:num)/lokasi_pantau/(:num)/titik_pantau/(:num)/save'] = 'Air/UkurAir/save/$1/$2/$3';
// END ROUTING DATA UKUR AIR //

// ROUTING DATA UKUR AIR DETAIL //
$route['air/data_air/detail/(:num)/lokasi_pantau/(:num)/titik_pantau/(:num)/data_ukur/(:num)'] = 'Air/UkurAirDetail/index/$1/$2/$3/$4';
// ROUTING END DATA UKUR AIR DETAIL //

// ROUTING PARAMETER UDARA //
$route['udara/parameter_udara'] = 'Udara/ParameterUkurUdara/index';
$route['udara/parameter_udara/tambah'] = 'Udara/ParameterUkurUdara/tambah';
$route['udara/parameter_udara/edit/(:num)'] = 'Udara/ParameterUkurUdara/edit/$1';
$route['udara/parameter_udara/delete/(:num)'] = 'Udara/ParameterUkurUdara/delete/$1';
$route['udara/parameter_udara/save'] = 'Udara/ParameterUkurUdara/save';
// END ROUTING PARAMETER UDARA //

// ROUTING LOKASI UDARA //
$route['udara/data_udara'] = 'Udara/LokasiUdara/index';
$route['udara/data_udara/tambah'] = 'Udara/LokasiUdara/tambah';
$route['udara/data_udara/edit/(:num)'] = 'Udara/LokasiUdara/edit/$1';
$route['udara/data_udara/delete/(:num)'] = 'Udara/LokasiUdara/delete/$1';
$route['udara/data_udara/save'] = 'Udara/LokasiUdara/save';
// END ROUTING LOKASI UDARA //

// ROUTING TITIK PANTAU //
$route['udara/data_udara/detail/(:num)'] = 'Udara/TitikUdara/index/$1';
$route['udara/data_udara/detail/(:num)/tambah'] = 'Udara/TitikUdara/tambah/$1';
$route['udara/data_udara/detail/(:num)/edit/(:num)'] = 'Udara/TitikUdara/edit/$1/$2';
// $route['udara/data_udara/detail/(:num)/lampiran/(:num)'] = 'Udara/TitikUdara/lampiran/$1/$2';
// $route['udara/data_udara/detail/(:num)/lampiran/(:num)/proses'] = 'Udara/TitikUdara/lampiranProses/$1/$2';
// $route['udara/data_udara/detail/(:num)/lampiran/(:num)/download'] = 'Udara/TitikUdara/lampiranDownload/$1/$2';
$route['udara/data_udara/detail/(:num)/delete/(:num)'] = 'Udara/TitikUdara/delete/$1/$2';
$route['udara/data_udara/detail/(:num)/save'] = 'Udara/TitikUdara/save';
// END ROUTING TITIK PANTAU //

// ROUTING DATA UKUR UDARA //
$route['udara/data_udara/detail/(:num)/titik_pantau/(:num)'] = 'Udara/UkurUdara/index/$1/$2';
$route['udara/data_udara/detail/(:num)/titik_pantau/(:num)/tambah'] = 'Udara/UkurUdara/tambah/$1/$2';
// $route['udara/data_udara/detail/(:num)/titik_pantau/(:num)/edit/(:num)'] = 'Udara/UkurUdara/edit/$1/$2/$3';
// $route['udara/data_udara/detail/(:num)/titik_pantau/(:num)/lampiran/(:num)'] = 'Udara/UkurUdara/lampiran/$1/$2/$3';
// $route['udara/data_udara/detail/(:num)/titik_pantau/(:num)/lampiran/(:num)/proses'] = 'Udara/UkurUdara/lampiranProses/$1/$2/$3';
// $route['udara/data_udara/detail/(:num)/titik_pantau/(:num)/lampiran/(:num)/download'] = 'Udara/UkurUdara/lampiranDownload/$1/$2/$3';
$route['udara/data_udara/detail/(:num)/titik_pantau/(:num)/delete/(:num)'] = 'Udara/UkurUdara/delete/$1/$2/$3';
$route['udara/data_udara/detail/(:num)/titik_pantau/(:num)/save'] = 'Udara/UkurUdara/save/$1/$2';
// END ROUTING DATA UKUR UDARA //

//  ROUTING DATA UKUR UDARA DETAIL //
$route['udara/data_udara/detail/(:num)/titik_pantau/(:num)/data_ukur/(:num)'] = 'Udara/UkurUdaraDetail/index/$1/$2/$3';
// END ROUTING DATA UKUR UDARA DETAIL //

$route['datatables/data_jenis_air'] = 'AjaxDatatables/dataJenisAir';